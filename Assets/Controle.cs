﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Controle : NetworkBehaviour
{
    Animator animP;
    float dumptime = 0.1f;
    
    // Start is called before the first frame update
    void Start()
    {
        animP = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (isLocalPlayer)
        {
            if (Input.GetKey(KeyCode.W))
            {
                animP.SetFloat("vertical", 1f, dumptime, Time.deltaTime);
                animP.SetBool("SHOOT", false);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                animP.SetFloat("vertical", -1f, dumptime, Time.deltaTime);
                animP.SetBool("SHOOT", false);
            }
            else
                animP.SetFloat("vertical", 0, dumptime, Time.deltaTime);

            if (Input.GetKey(KeyCode.A))
            {
                animP.SetFloat("horizontal", -1, dumptime, Time.deltaTime);
                animP.SetBool("SHOOT", false);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                animP.SetFloat("horizontal", 1, dumptime, Time.deltaTime);
                animP.SetBool("SHOOT", false);
            }
            else
                animP.SetFloat("horizontal", 0);

            if (Input.GetKey(KeyCode.Space))
                animP.SetBool("SHOOT", true);
            else
                animP.SetBool("SHOOT", false);
        }

        
       
        
        

    }
}
