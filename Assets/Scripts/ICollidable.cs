﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//for every object that collides and changes player values
public interface ICollidable
{
    int Value();
}