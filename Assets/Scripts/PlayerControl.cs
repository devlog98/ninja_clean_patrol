﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlayerControl : NetworkBehaviour {
    Rigidbody rb;

    [SerializeField] float vel = 10f;
    [SerializeField] float rotacaoVel = 50f;

    [SerializeField] GameObject tiroPrefab;
    [SerializeField] Transform firePoint;
    [SerializeField] float tiroVel = 10f;
    [SerializeField] float tiroDuracao = 2f;

    bool alive = true;
    [SerializeField] GameObject model;

    //HUD
    GameObject playerHUD;

    public Vector3 spawnPos;

    [SerializeField] Slider sliderPrefab;
    Slider playerHealth;
    [SyncVar(hook = "AlteraVida")] int vida = 100;    

    Vector3 initialScale;
    [SerializeField] float objectScale = 1f;

    public int lives;
    public GameObject GM;

    //sync var makes server updates every same object in local machines
    [SyncVar(hook = "TrocaCor")] public Color corPlayer;
    public string playerName;

    void Start() {
        GM = GameObject.Find("GM");
        rb = GetComponent<Rigidbody>();

        spawnPos = transform.position;

        if (isLocalPlayer) {
            CameraFollow360.player = transform;
        }

        playerHUD = GameObject.Find("PlayerHUD");
        playerHealth = Instantiate(sliderPrefab, playerHUD.transform);

        initialScale = transform.localScale;

        if (isServer) {
            CmdCTrocaCor(corPlayer);
        }
        else if (!isLocalPlayer) {
            TrocaCor(corPlayer);
        }
    }

    void OnDestroy() {
        Destroy(playerHealth.gameObject);
    }

    void Update() {
        if (isLocalPlayer && alive) {
            float y = Input.GetAxis("Horizontal") * Time.deltaTime * rotacaoVel;
            float z = Input.GetAxis("Vertical") * Time.deltaTime * vel;

            rb.transform.Rotate(0, y, 0);
            rb.transform.Translate(0, 0, z);

            if (Input.GetKeyDown(KeyCode.Mouse0)) {
                CmdAtirar();
            }
        }

        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        playerHealth.transform.position = pos;

        Plane plane = new Plane(Camera.main.transform.forward, Camera.main.transform.position);
        float dist = plane.GetDistanceToPoint(transform.position);
        playerHealth.transform.localScale = initialScale * objectScale / dist;
    }

    void AlteraVida(int v) {
        vida = v;
        playerHealth.value = vida;
    }

    void OnCollisionEnter(Collision collision) {
        ICollidable obj = collision.gameObject.GetComponent<ICollidable>();
        if (obj != null && isServer) {
            CmdAlteraVida(obj.Value());
        }
    }

    [Command]
    void CmdAlteraVida(int v) {
        vida += v;

        if (vida == 0) {
            StopCoroutine(this.RespawnCoroutine());
            StartCoroutine(this.RespawnCoroutine());
        }
    }

    IEnumerator RespawnCoroutine() {
        Debug.Log("Death");
        alive = false;
        model.gameObject.SetActive(false);
        playerHealth.gameObject.SetActive(false);
        lives--;

        yield return new WaitForSeconds(1f);

        if (lives > 0) {
            Debug.Log("Life");
            transform.position = spawnPos;
            alive = true;
            model.gameObject.SetActive(true);
            playerHealth.gameObject.SetActive(true);
            CmdAlteraVida(100);
        }
        else {
            GM.GetComponent<MatchManager>().RemovePlayerFromList();
        }
    }

    void Atirar() {
        GameObject bullet = Instantiate(tiroPrefab, firePoint.position, firePoint.transform.rotation);
        bullet.GetComponent<Rigidbody>().velocity = firePoint.transform.forward * tiroVel;
        Destroy(bullet, tiroDuracao);
    }

    //RPC -> Remote Procedure Call
    //chama método em uma máquina remota
    //Cmd e ClientRpc são RPCs

    //cliente manda para servidor
    //todo comando tem que começar com "Cmd"
    [Command]
    void CmdAtirar() {
        RpcAtirar();
    }

    //servidor manda para clientes
    //precisa ter "Rpc" no começo
    [ClientRpc]
    void RpcAtirar() {
        Atirar();
    }

    [Command]
    void CmdCTrocaCor(Color c) {
        //model.GetComponent<Renderer>().material.color = c;
    }

    void TrocaCor(Color c) {
        //model.GetComponent<Renderer>().material.color = c;
    }
}