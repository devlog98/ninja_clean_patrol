﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MatchManager : MonoBehaviour
{
    // Time management
    [SerializeField] int timer;
    public Text timerText;

    // Alive Player Management
    public List<GameObject> players;

    public GameObject victoryScreen;
    public Text returningText;
    public float waitTime;

    private void Start()
    {
        StartCoroutine(CountdownCoroutine());
        StartCoroutine(WaitForPlayers());
    }

    public int Timer {
        get { return timer; }
        set { timer = value; }
    }

    private void Update()
    {
        if(players.Count == 1)
        {
            victoryScreen.GetComponent<Text>().text = players[0].gameObject.GetComponent<PlayerControl>().playerName + " VENCEU";
            returningText.text = "Retornando ao Lobby em " + waitTime.ToString("0");
            victoryScreen.gameObject.SetActive(true);

            if (waitTime < 0)
                SceneManager.LoadScene(0);
            else
                waitTime -= 1 * Time.deltaTime;

            //StartCoroutine(backToMenu);
        }
    }

    public void RemovePlayerFromList()
    {
        foreach(GameObject player in players)
        {
            if(player.GetComponent<PlayerControl>().lives == 0)
            {
                Debug.Log("mata o player");
                players.Remove(player);
            }
        }
    }

    IEnumerator WaitForPlayers()
    {
        yield return new WaitForSeconds(2);

        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            players.Add(player);

            if (player.GetComponent<PlayerControl>().lives == 0)
            {
                players.Remove(player);
            }
        }

    }

    // Countdown to Endgame
    IEnumerator CountdownCoroutine()
    {
        while (timer != 0)
        {
            timer--;
            timerText.text = "MATCH TIME: " + timer.ToString();
            yield return new WaitForSeconds(1);
        }
        if(timer == 0)
        {
            EndGame();
        }
    }

    // Finish everything
    public void EndGame()
    {
        Time.timeScale = 0f;
        Debug.Log("EndGame");
    }
}
