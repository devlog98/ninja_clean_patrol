﻿using Prototype.NetworkLobby;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerLobbySetup : LobbyHook
{
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        gamePlayer.GetComponent<PlayerControl>().corPlayer = lobbyPlayer.GetComponent<LobbyPlayer>().playerColor;
        gamePlayer.GetComponent<PlayerControl>().playerName = lobbyPlayer.GetComponent<LobbyPlayer>().playerName;
    }
}
